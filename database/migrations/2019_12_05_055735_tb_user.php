<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropifExists('tb_user');
        Schema::create('tb_user', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('username', 100)->unique();
            $table->string('password', 100);
            $table->string('email', 100);
            $table->integer('id_jabatan')->unsigned();
            $table->foreign('id_jabatan')->references('id_jabatan')->on('tb_jabatan');
            $table->string('level', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user');
    }
}
