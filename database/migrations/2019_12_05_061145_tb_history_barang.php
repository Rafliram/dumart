<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHistoryBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropifExists('tb_history_barang');
        Schema::create('tb_history_barang', function (Blueprint $table) {
            $table->increments('id_history');
            $table->string('kode_barang', 100)->unsigned();
            $table->foreign('kode_barang')->references('kode_barang')->on('tb_barang');
            $table->string('nama_barang', 100);
            $table->integer('stok_barang');
            $table->integer('harga_barang');
            $table->string('status', 100);
            $table->string('barcode', 100);
            $table->integer('id_kategori')->unsigned();
            $table->foreign('id_kategori')->references('id_kategori')->on('tb_kategori');
            $table->date('tgl_input');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_history_barang');
    }
}
